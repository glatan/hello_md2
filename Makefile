NAME = md2

.PHONE: clear
clear: ${NAME}
	@rm ${NAME}

.PHONY: go
go:
	@go run ${NAME}.go

.PHONY: ruby
.ruby:
	@ruby ${NAME}.rb

.PHONY: rust
rust:
	@rustc ${NAME}.rs
	@./${NAME}
