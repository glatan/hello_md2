package main

import (
	"bufio"
	"fmt"
	"os"
)

const BLKSIZE int = 16

var STABLE = [256]byte{
	41, 46, 67, 201, 162, 216, 124, 1, 61, 54, 84, 161, 236, 240, 6, 19, 98, 167, 5, 243, 192, 199,
	115, 140, 152, 147, 43, 217, 188, 76, 130, 202, 30, 155, 87, 60, 253, 212, 224, 22, 103, 66,
	111, 24, 138, 23, 229, 18, 190, 78, 196, 214, 218, 158, 222, 73, 160, 251, 245, 142, 187, 47,
	238, 122, 169, 104, 121, 145, 21, 178, 7, 63, 148, 194, 16, 137, 11, 34, 95, 33, 128, 127, 93,
	154, 90, 144, 50, 39, 53, 62, 204, 231, 191, 247, 151, 3, 255, 25, 48, 179, 72, 165, 181, 209,
	215, 94, 146, 42, 172, 86, 170, 198, 79, 184, 56, 210, 150, 164, 125, 182, 118, 252, 107, 226,
	156, 116, 4, 241, 69, 157, 112, 89, 100, 113, 135, 32, 134, 91, 207, 101, 230, 45, 168, 2, 27,
	96, 37, 173, 174, 176, 185, 246, 28, 70, 97, 105, 52, 64, 126, 15, 85, 71, 163, 35, 221, 81,
	175, 58, 195, 92, 249, 206, 186, 197, 234, 38, 44, 83, 13, 110, 133, 40, 132, 9, 211, 223, 205,
	244, 65, 129, 77, 82, 106, 220, 55, 200, 108, 193, 171, 250, 36, 225, 123, 8, 12, 189, 177, 74,
	120, 136, 149, 139, 227, 99, 232, 109, 233, 203, 213, 254, 59, 0, 29, 57, 242, 239, 183, 14,
	102, 88, 208, 228, 166, 119, 114, 248, 235, 117, 75, 10, 49, 68, 80, 180, 143, 237, 31, 26,
	219, 153, 141, 51, 159, 17, 131, 20,
}

type Md2Ctx struct {
	word_block []byte
	state      [48]byte
}

func NewMd2Ctx(input []byte) *Md2Ctx {
	return &Md2Ctx{
		word_block: input,
		state:      [48]byte{},
	}
}

func (self *Md2Ctx) padding() {
	var message_length int = len(self.word_block)
	var padding_byte byte = byte(BLKSIZE - (message_length % BLKSIZE))
	for i := 0; i < int(padding_byte); i++ {
		self.word_block = append(self.word_block, padding_byte)
	}
}

func (self *Md2Ctx) add_checksum() {
	var word_block_length int = len(self.word_block) / BLKSIZE
	var checksum []byte = make([]byte, 16)
	var c int
	var l int = 0
	for i := 0; i < word_block_length; i++ {
		for j := 0; j < 16; j++ {
			c = int(self.word_block[16*i+j])
			checksum[j] ^= STABLE[c^l]
			l = int(checksum[j])
		}
	}
	self.word_block = append(self.word_block, checksum...)
}

func (self *Md2Ctx) round() {
	var word_block_length int = len(self.word_block) / BLKSIZE
	var t int = 0
	for i := 0; i < word_block_length; i++ {
		for j := 0; j < 16; j++ {
			self.state[j+16] = self.word_block[16*i+j]
			self.state[j+32] = self.state[j+16] ^ self.state[j]
		}
		t = 0
		for j := 0; j < 18; j++ {
			for k := 0; k < 48; k++ {
				self.state[k] ^= STABLE[t]
				t = int(self.state[k])
			}
			t = (t + j) % 256
		}
	}
}

func digest(input []byte) string {
	var md2ctx = NewMd2Ctx(input)
	md2ctx.padding()
	md2ctx.add_checksum()
	md2ctx.round()
	return string(md2ctx.state[0:16])
}

func main() {
	var stdin = bufio.NewScanner(os.Stdin)
	fmt.Print("In: ")
	for stdin.Scan() {
		var md2hash = digest([]byte(stdin.Text()))
		fmt.Printf("MD2: %x\n\nIn: ", md2hash)
	}
}
