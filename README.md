# About

Hello, Worldの代わりに実装されたMD2がまとめられたリポジトリです。

## File

|Language|Filename|
|-|-|
|Go|[md2.go](./md2.go)|
|Ruby|[md2.rb](./md2.rb)|
|Rust|[md2.rs](./md2.rs)|

## Makefile
```bash
make ${LANG NAME}

# Example
make go
```

## Reference

https://tools.ietf.org/html/rfc1319
