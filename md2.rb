class Md2Ctx
    def initialize(input)
        @@BLOCK_SIZE = 16
        @@STABLE = [
            41, 46, 67, 201, 162, 216, 124, 1, 61, 54, 84, 161, 236, 240, 6, 19, 98, 167, 5, 243, 192, 199,
            115, 140, 152, 147, 43, 217, 188, 76, 130, 202, 30, 155, 87, 60, 253, 212, 224, 22, 103, 66,
            111, 24, 138, 23, 229, 18, 190, 78, 196, 214, 218, 158, 222, 73, 160, 251, 245, 142, 187, 47,
            238, 122, 169, 104, 121, 145, 21, 178, 7, 63, 148, 194, 16, 137, 11, 34, 95, 33, 128, 127, 93,
            154, 90, 144, 50, 39, 53, 62, 204, 231, 191, 247, 151, 3, 255, 25, 48, 179, 72, 165, 181, 209,
            215, 94, 146, 42, 172, 86, 170, 198, 79, 184, 56, 210, 150, 164, 125, 182, 118, 252, 107, 226,
            156, 116, 4, 241, 69, 157, 112, 89, 100, 113, 135, 32, 134, 91, 207, 101, 230, 45, 168, 2, 27,
            96, 37, 173, 174, 176, 185, 246, 28, 70, 97, 105, 52, 64, 126, 15, 85, 71, 163, 35, 221, 81,
            175, 58, 195, 92, 249, 206, 186, 197, 234, 38, 44, 83, 13, 110, 133, 40, 132, 9, 211, 223, 205,
            244, 65, 129, 77, 82, 106, 220, 55, 200, 108, 193, 171, 250, 36, 225, 123, 8, 12, 189, 177, 74,
            120, 136, 149, 139, 227, 99, 232, 109, 233, 203, 213, 254, 59, 0, 29, 57, 242, 239, 183, 14,
            102, 88, 208, 228, 166, 119, 114, 248, 235, 117, 75, 10, 49, 68, 80, 180, 143, 237, 31, 26,
            219, 153, 141, 51, 159, 17, 131, 20
        ]
        @word_block = input
        @state = Array.new(48, 0)
    end
    def Padding()
        message_length = @word_block.length / @@BLOCK_SIZE
        padding_byte = (@@BLOCK_SIZE - (message_length % @@BLOCK_SIZE))
        @word_block.concat(Array.new(padding_byte, padding_byte))
    end
    def AddCheckSum()
        word_block_length = @word_block.length / @@BLOCK_SIZE
        checksum = Array.new(16, 0)
        l = 0
        for i in 0..(word_block_length - 1) do
            for j in 0..(16 - 1) do
                c = @word_block[16 * i + j]
                checksum[j] ^= @@STABLE[c ^ l]
                l = checksum[j]
            end
        end
        @word_block.concat(checksum)
    end
    def Round()
        word_block_length = @word_block.length / @@BLOCK_SIZE
        for i in 0..(word_block_length - 1) do
            for j in 0..(16 - 1) do
                @state[j + 16] = @word_block[16 * i + j]
                @state[j + 32] = @state[j+ 16] ^ @state[j]
            end
            t = 0
            for j in 0..(18 - 1) do
                for k in 0..(48 - 1) do
                    @state[k] ^= @@STABLE[t]
                    t = @state[k]
                end
                t = (t + j) % 256
            end
        end
    end
    def self.Digest(input)
        md2ctx = self.new(input)
        md2ctx.Padding()
        md2ctx.AddCheckSum()
        md2ctx.Round()
        return md2ctx.instance_variable_get(:@state)[0, 16].map{|c| c.to_s(16)}.join("")
    end
end

while true
    print "In: "
    input = gets.chomp
    print "MD2: #{Md2Ctx.Digest(input.bytes)}\n\n"
end
